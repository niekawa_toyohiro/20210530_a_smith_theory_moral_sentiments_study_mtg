# 20210530_A_SMITH_theory_moral_sentiments_study_mtg

This project is for a private study meeting on Adam Smith’s The Theory of Moral Sentiments, spinning off from Sanmonkai (the morning study meeting held in Tokyo, Japan).  
アダム・スミスの『道徳感情論』を読む私的な勉強会のためのプロジェクトです。本勉強会は、日本国の東京で開催されている朝活勉強会の三文会のスピンオフ企画です。

資料は、左側のメニューの「Wiki」および電子ファイル保管サービスBoxの [https://app.box.com/s/vh36t166316ja9usltufxyomojqd6tfa](https://app.box.com/s/vh36t166316ja9usltufxyomojqd6tfa) にまとめました。
